var express = require('express');
var app = express();
let port = 3001;
var session = require('express-session');
var path = require('path');
var passport = require('passport');
var morgan  = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var models = require("./app/models");
var compression = require('compression');
var helmet = require('helmet');
var authcontroller = require("./app/controllers/authcontrollers");

app.locals.moment = require('moment');

app.set('view engine', 'pug')
app.set('views','./app/views');

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.json()); // get information from html forms
app.use(bodyParser.urlencoded({ extended: true }));

//Set Public Folder
app.use(express.static('public'));

//Set Node Modules Folder
app.use('/node_modules', express.static(__dirname + '/node_modules'));

app.use(session({
    secret: 'MYSECRETISVERYSECRET',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(compression()); //Compress all routes
app.use(helmet());

require('./app/config/passport/passport.js')(passport, models.user); // pass passport for configuration

var authRoute = require('./app/routes/auth.js')(app,passport);

models.sequelize.sync().then(function() {
    console.log('Nice! Database looks fine')
    //Chuck this in to encrypt a password on startup
    //authcontroller.globchangepass(1, 'test1')
 }).catch(function(err) {
    console.log(err, "Something went wrong with the Database Update!")
 });
 
 app.listen(port, function(error){
    if(error){
        return console.log('There has been an error', error);
    }
    console.log('Server is listening on port', port)
});