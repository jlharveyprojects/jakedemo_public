//route middleware to ensure user is logged in
isLoggedIn = function(req, res, next) {
    if (req.isAuthenticated()) {
        console.log("Authenticated!")
        next();
    } else {
        console.log('Not Authenticated')
        res.redirect("/login");
    }
}

module.exports.isLoggedIn = isLoggedIn;

//route middleware to ensure user is admin
isAdmin = function(req, res, next) {
    if (req.user && req.user.admin == 'Y') {
        console.log("Admin Login Authenticated!")
        return next();
    } else {
        console.log("Attempted Admin Access!")
        res.send(401, 'Unauthorized');
    }
}

module.exports.isAdmin = isAdmin;