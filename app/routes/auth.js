var authController = require('../controllers/authcontrollers.js');
var flash = require(`connect-flash`);
var session = require('express-session');
var BetterMemoryStore = require('session-memory-store')(session);
var store = new BetterMemoryStore({ expires: 60 * 60 * 1000, debug: true });

var locationController = require('../controllers/locationcontroller');
var contactController = require('../controllers/contactcontroller');
var taskController = require('../controllers/taskcontroller');
var presentationController = require('../controllers/presentationcontroller');
var authfunctions = require('./authfunctions.js');

module.exports = function (app, passport) {

    app.use(session({
        name: 'JSESSION',
        secret: 'MYSECRETISVERYSECRET',
        store: store,
        resave: true,
        saveUninitialized: true
    }));
    app.use(flash());

    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/locations',
        failureRedirect: '/login',
        failureFlash: true
    }));

    app.get('/login', authController.login);

    app.get('/changepwd', authfunctions.isLoggedIn, authController.changepwdpage);

    app.post('/changepassword', authfunctions.isLoggedIn, authController.changepassword);

    app.get('/locations', authfunctions.isLoggedIn, locationController.locations);

    app.post('/locations/details', authfunctions.isLoggedIn, locationController.view_location);

    app.post('/new', authfunctions.isLoggedIn, authController.new_entity);

    app.post('/newlocation', authfunctions.isLoggedIn, locationController.new_location);

    app.post('/editlocation', authfunctions.isLoggedIn, locationController.edit_location);

    app.get('/contacts', authfunctions.isLoggedIn, contactController.contacts);

    app.post('/contacts/details', authfunctions.isLoggedIn, contactController.view_contact);

    app.post('/newcontact', authfunctions.isLoggedIn, contactController.new_contact);
    
    app.post('/editcontact', authfunctions.isLoggedIn, contactController.edit_contact);

    app.get('/tasks', authfunctions.isLoggedIn, taskController.tasks);

    app.post('/tasks/details', authfunctions.isLoggedIn, taskController.view_task);

    app.post('/taskscal', authfunctions.isLoggedIn, taskController.taskscal);

    app.get('/taskCalendarJSON/', authfunctions.isLoggedIn, taskController.tasksJSON);

    app.post('/newtask', authfunctions.isLoggedIn, taskController.new_task);
    
    app.post('/edittask', authfunctions.isLoggedIn, taskController.edit_task);

    app.get('/presentations', authfunctions.isLoggedIn, presentationController.presentations);

    app.post('/presentations/details', authfunctions.isLoggedIn, presentationController.view_presentation);

    app.post('/presentationscal', authfunctions.isLoggedIn, presentationController.presentationscal);

    app.get('/presentationCalendarJSON/', authfunctions.isLoggedIn, presentationController.presentationsJSON);

    app.post('/newpresentation', authfunctions.isLoggedIn, presentationController.new_presentation);
    
    app.post('/editpresentation', authfunctions.isLoggedIn, presentationController.edit_presentation);

    app.post('/deleteentity', authfunctions.isLoggedIn, authController.delete_entity);

    app.get('/careplans', authfunctions.isLoggedIn && authfunctions.isAdmin, (req, res) => {
        console.log(req.session);
        console.log("Hit /auth/careplans");
        res.render('deadend.pug', {
            user: req.user
        });
        ;
    });

    app.get('/profile', authfunctions.isLoggedIn && authfunctions.isAdmin, (req, res) => {
        console.log(req.session);
        console.log("Hit /auth/profile");
        res.render('deadend.pug', {
            user: req.user
        });
        ;
    });

    app.get('/admin', authfunctions.isLoggedIn && authfunctions.isAdmin, (req, res) => {
        console.log(req.session);
        console.log("Hit /auth/admin");
        res.render('deadend.pug', {
            user: req.user
        });
        ;
    });

    app.get('/logout',authController.logout);
}
