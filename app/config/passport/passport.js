//load bcrypt
var bCrypt = require('bcrypt-nodejs');
var modelcontroller = require("../../controllers/modelcontroller.js");

module.exports = function (passport, user) {

    var User = user;
    var LocalStrategy = require('passport-local').Strategy;

    //serialize
    passport.serializeUser(function (user, done) {
        done(null, user.id);
        console.log("User ID Serialized:" + user.id);
    });

    // deserialize user 
    passport.deserializeUser(function (id, done) {
        User.findByPk(id).then(function (user) {
            if (user) {
                done(null, user.get());
                console.log("User ID De-Serialized:" + user.id);
            } else {
                done(user.errors, null);
            }
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true //passback entire req to call back
    }, function (req, email, password, done) {
        var isValidPassword = function (userpass, password) {
            return bCrypt.compareSync(password, userpass);
        }
        let date = new Date().toLocaleString('en-GB');
        console.log(date);
        User.findOne({
            where: {
                Email: email
            }
        }).then(function (user) {
            if (!user) {
                console.log("Passport No User returned");
                return done(null, false, req.flash('loginMessage', 'That email address does not exist'), req.flash('email', email));
            }
            if (!isValidPassword(user.password, password)) {
                console.log("bad passwd!");
                modelcontroller.LoginHistory.create(
                    {
                        timestamp: Date(),
                        userid: user.id,
                        successflag: 'N'
                    }
                );
                return done(null, false, req.flash('loginMessage', 'That password is incorrect'), req.flash('email', email));
            }
            var userinfo = user.get();
            modelcontroller.LoginHistory.create(
                {
                    timestamp: Date(),
                    userid: userinfo.id,
                    successflag: 'Y'
                }
            );
            return done(null, userinfo);

        }).catch(function (err) {
            console.log("Error:", err);
            return done(null, false, req.flash('loginMessage', 'Something went wrong, please speak to the Administrator'), req.flash('email', email));
        });
    }
    ));
}