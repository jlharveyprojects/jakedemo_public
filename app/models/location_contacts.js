var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var location_contacts= sequelize.define('location_contacts', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Department: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Email: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Phone: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SecName: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SecPhone: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    SecEmail: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Type: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
  }, {
    tableName: 'location_contacts'
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Location_contacts.belongsTo(models.Locations, {
          foreignKey: 'Locationid',
          onDelete: 'CASCADE'
        });
      }
    }
  });
  return location_contacts;
};
