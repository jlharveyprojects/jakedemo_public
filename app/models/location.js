var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var Locations = sequelize.define('locations', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Name: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Trust: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Postcode: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Telephone: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
        defaultValue: Sequelize.NOW
    }
  }, {
    tableName: 'locations'
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Locations.belongsTo(models.hlc, {
          foreignKey: 'hlcid',
          onDelete: 'CASCADE'
        });
        Locations.hasMany(models.location_contacts,{
          foreignKey: 'id',
	        onDelete: 'CASCADE'
        });
      }
    }
  });
  return Locations;
};
