/* jshint indent: 2 */
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('presentations', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Datetime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Locationid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    Locationcontactid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    type: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    instructions: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    }
  }, {
    tableName: 'presentations'
  });
};
