/* jshint indent: 2 */
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('loginhistory', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: true
    },
    userid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    successflag: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    }
  }, {
    tableName: 'loginhistory'
  });
};
