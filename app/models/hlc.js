var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var hlc = sequelize.define('hlc', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    tableName: 'hlc'
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        hlc.hasMany(models.user,{
          foreignKey: 'hlcid',
	        onDelete: 'CASCADE'
        });
        hlc.hasMany(models.locations,{
          foreignKey: 'hlcid',
	        onDelete: 'CASCADE'
        });
      }
    }
  });
  return hlc;
};
