var Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('user', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Firstname: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Lastname: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Email: {
      type: DataTypes.TEXT,
      allowNull: true,
      validate: {
        isEmail: true
    }
    },
    Telephone: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    password: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    admin: {
      type: DataTypes.TEXT,
      allowNull: true,
      defaultValue: '\'N\''
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    }
  }, {
    tableName: 'users'
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        User.belongsTo(models.hlc, {
          foreignKey: 'hlcid',
          onDelete: 'CASCADE'
        });
      }
    }
  });
  return User;
};
