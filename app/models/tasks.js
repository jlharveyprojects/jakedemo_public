/* jshint indent: 2 */
var Sequelize = require("sequelize");
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tasks', {
    id: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    duedate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    Locationid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    Locationcontactid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    action: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    type: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    notes: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    userid: {
      type: DataTypes.INTEGER(11).UNSIGNED,
      allowNull: true
    },
    createdAt: {
        field: 'created_at',
        type: Sequelize.DATE,
    },
    updatedAt: {
        field: 'updated_at',
        type: Sequelize.DATE,
    }
  }, {
    tableName: 'tasks'
  });
};
