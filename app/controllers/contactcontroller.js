var exports = module.exports = {}
var models = require("../models");
var modelcontroller = require("./modelcontroller");
var authController = require('./authcontrollers');

//Location Contacts Page
exports.contacts = function (req, res) {
    modelcontroller.Location_Contacts.findAll({
        include: [{
            model: modelcontroller.Locations,
            where: {
                hlcid: req.user.hlcid
            }
        }]
    }).then(function (contacts) {
        res.render('contacts.pug', {
            successmessages: req.flash('successMessage'),
            failmessages: req.flash('failMessage'),
            user: req.user,
            name: 'Contacts',
            data: contacts
        })
    });
    console.log("Hit /auth/contacts");
}

var view_contact = function (req, res, data) {
    console.log("Hit View Contacts");
    let type = 'Contact';
    let render = function (req, givenid) {
        if (givenid & !req.body.Id) {
            modelcontroller.Location_Contacts.findOne({
                where: {
                    id: givenid
                },
                include: {
                    model: modelcontroller.Locations
                }
            }).then(function (data1) {
                modelcontroller.Locations.findAll({
                    attributes: ['Name', 'id'],
                    order: ['Name']
                }).then(function (locationdata) {
                    console.log(data1);
                    console.log(data1.location.Name);
                    res.render('new.pug', {
                        successmessages: req.flash('successMessage'),
                        failmessages: req.flash('failMessage'),
                        user: req.user,
                        name: 'View',
                        type,
                        contacts: data1,
                        locationdata
                    });
                })
            })
        }
        else {
            modelcontroller.Location_Contacts.findOne({
                where: {
                    id: req.body.Details
                },
                include: {
                    model: modelcontroller.Locations
                }
            }).then(function (data1) {
                modelcontroller.Locations.findAll({
                    attributes: ['Name', 'id'],
                    order: ['Name']
                }).then(function (locationdata) {
                    console.log(data1);
                    console.log(data1.location.Name);
                    res.render('new.pug', {
                        successmessages: req.flash('successMessage'),
                        failmessages: req.flash('failMessage'),
                        user: req.user,
                        name: 'View',
                        type,
                        contacts: data1,
                        locationdata
                    });
                })
            })
        }
    }
    render(req, data.id);
    console.log(req.session);
}

//View Contact Page
exports.view_contact = view_contact;

//Edit Contact Page
exports.edit_contact = function (req, res) {
    console.log("Hit Edit Contacts");
    let type = 'Contact';
    modelcontroller.Location_Contacts.findOne({
        where: {
            id: req.body.Id
        },
        include: {
            model: modelcontroller.Locations
        }
    }).then(function (data1) {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: { hlcid: req.user.hlcid }
        }).then(function (locationdata) {
            console.log(data1);
            console.log(data1.location.Name);
            res.render('new.pug', {
                successmessages: req.flash('successMessage'),
                failmessages: req.flash('failMessage'),
                user: req.user,
                name: 'Edit',
                type,
                contacts: data1,
                locationdata
            });
        })
    })
    console.log(req.session);
}

//New Contact Post
exports.new_contact = function (req, res) {
    console.log('New Contact Process #1 ' + req.body.Id);
    var existingid = '';
    modelcontroller.Location_Contacts
        .findOne({ where: { Name: req.body.Name } }).then(function (data) {
            if (data) { existingid = data.id };
            if (String(req.body.Id) !== String(existingid)) {
                console.log('failMessage', `Contact ${data.Name} already exists.`);
                console.log(data.id);
                req.flash('failMessage', `Contact ${data.Name} already exists.`);
                view_contact(req, res, data);
            }
            else if (String(req.body.Location) == String('--Location')) {
                console.log('failMessage', `Location "--Location" is invalid.`);
                req.flash('failMessage', `Location "--Location" is invalid.`);
                res.redirect('/contacts');
            }
            else {
                ContactsUpsert({
                    id: req.body.Id,
                    Name: req.body.Name,
                    Title: req.body.Title,
                    Department: req.body.Department,
                    Email: req.body.Email,
                    Phone: req.body.Phone,
                    SecName: req.body.SecName,
                    SecPhone: req.body.SecPhone,
                    SecEmail: req.body.SecEmail,
                    Type: req.body.Type,
                    Locationid: req.body.Location
                },
                    {
                        id: req.body.Id
                    }).then(newContact => {
                        req.flash('successMessage', `Contact ${newContact.Name} has been Created/Updated.`);
                        res.redirect('/contacts');
                        console.log(`Contact ${newContact.Name}, with id ${newContact.id} has been Created/Updated.`);
                    });
            }
        })
}

function ContactsUpsert(values, condition) {
    return modelcontroller.Location_Contacts
        .findOne({ where: condition })
        .then(function (obj) {
            if (obj) { // update
                return obj.update(values);
            }
            else { // insert
                return modelcontroller.Location_Contacts.create(values);
            }
        })
};