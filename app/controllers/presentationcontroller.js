var exports = module.exports = {}
var modelcontroller = require("./modelcontroller");
var moment = require('moment');

//Presentations Page
exports.presentations = function (req, res) {
    modelcontroller.Presentations.findAll({
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (presentations) {
        console.log(presentations);
        res.render('presentations.pug', {
            successmessages: req.flash('successMessage'),
            failmessages: req.flash('failMessage'),
            user: req.user,
            name: 'Presentations',
            presentations
        })
    });
    console.log(req.session);
    console.log("Hit /auth/presentations");
}

//PresentationsCal Page
exports.presentationscal = function (req, res) {
    res.render('presentations.pug', {
        successmessages: req.flash('successMessage'),
        failmessages: req.flash('failMessage'),
        user: req.user,
        name: 'Presentations',
        type: 'Calendar'
    })
    console.log(req.session);
    console.log("Hit /auth/presentationscal");
};

//Calendar JSON Server
exports.presentationsJSON = function (req, res) {
    modelcontroller.Presentations.findAll({
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (presentations) {
        events = [];
        for (presentation in presentations) {
            events.push({
                "id": presentations[presentation].id,
                "title": presentations[presentation].location_contact.Name,
                "start": presentations[presentation].Datetime,
                "textColor": '#000000'
            })
        }
    });
    res.json(events);
}

var view_presentation = function (req, res, data) {
    console.log("Hit View Presentation");
    let type = 'Presentation';
    let render = function (req, givenid) {
        if (givenid & !req.body.Id) {
            modelcontroller.Presentations.findOne({
                where: {
                    id: givenid
                },
                include: [
                    {
                        model: modelcontroller.Locations,
                        where: {
                            hlcid: req.user.hlcid
                        },
                    },
                    {
                        model: modelcontroller.Location_Contacts
                    },
                    {
                        model: modelcontroller.Users
                    }
                ]
            }).then(function (presentations) {
                console.log(presentations.location.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    presentations
                });
            })
        }
        else {
            modelcontroller.Presentations.findOne({
                where: {
                    id: req.body.Details
                },
                include: [
                    {
                        model: modelcontroller.Locations,
                        where: {
                            hlcid: req.user.hlcid
                        },
                    },
                    {
                        model: modelcontroller.Location_Contacts
                    },
                    {
                        model: modelcontroller.Users
                    }
                ]
            }).then(function (presentations) {
                console.log(presentations.location.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    presentations
                });
            })
        }
    }
    render(req, data.id);
    console.log(req.session);
}

//View Presentation Page
exports.view_presentation = view_presentation;

//Edit Contact Page
exports.edit_presentation = function (req, res) {
    console.log("Hit Edit presentation");
    let type = 'Presentation';
    modelcontroller.Presentations.findOne({
        where: {
            id: req.body.Id
        },
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (presentations) {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: {
                hlcid: req.user.hlcid
            }
        }).then(function (locationdata) {
            modelcontroller.Location_Contacts.findAll({
                attributes: ['Name', 'id'],
                order: ['Name']
            }).then(function (contactdata) {
                modelcontroller.Users.findAll({
                    order: ['Firstname'],
                    where: {
                        hlcid: req.user.hlcid
                    }
                }).then(function (userdata) {
                    res.render('new.pug', {
                        successmessages: req.flash('successMessage'),
                        failmessages: req.flash('failMessage'),
                        user: req.user,
                        name: 'Edit',
                        type,
                        presentations,
                        locationdata,
                        contactdata,
                        userdata
                    });
                })
            })
        })
    })
    console.log(req.session);
}

//New Presentation Post
exports.new_presentation = function (req, res) {
    console.log('New Presentation Process #1 ' + req.body.Id);
    PresentationsUpsert({
        id: req.body.Id,
        Locationid: req.body.Location,
        Locationcontactid: req.body.Contact,
        type: req.body.Type,
        instructions: req.body.Instructions,
        Datetime: moment.utc(req.body.Datetime),
        userid: req.body.User
    },
        {
            id: req.body.Id
        }).then(newPresentation => {
            console.log('Datetime is' + moment.utc(newPresentation.Datetime))
            req.flash('successMessage', `Presentation ${newPresentation.type} has been Created/Updated.`);
            res.redirect('/presentations');
            console.log(`Presentation ${newPresentation.type}, with id ${newPresentation.id} has been Created/Updated.`);
        });
}

function PresentationsUpsert(values, condition) {
    return modelcontroller.Presentations
        .findOne({ where: condition })
        .then(function (obj) {
            if (obj) { // update
                console.log('Updating Presentation Process #1 ')
                return obj.update(values);
            }
            else { // insert
                console.log('Updating Presentation Process #2 ')
                return modelcontroller.Presentations.create(values);
            }
        })
};