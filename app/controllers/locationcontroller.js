var exports = module.exports = {}
var models = require("../models");
var modelcontroller = require("./modelcontroller");

//Locations Page
exports.locations = function (req, res) {
    modelcontroller.Locations.findAll({
        where: {
            hlcid: req.user.hlcid
        }
    }).then(function (locations) {
        res.render('locations.pug', {
            successmessages: req.flash('successMessage'),
            failmessages: req.flash('failMessage'),
            user: req.user,
            name: 'Locations',
            data: locations
        })
    });
    console.log(req.session);
    console.log("Hit /auth/locations");
}

//View Location Page
var view_location = function (req, res, data) {
    console.log("Hit View Locations");
    let type = 'Location';
    let render = function (req, givenid) {
        if (givenid) {
            modelcontroller.Locations.findOne({
                where: {
                    id: givenid
                }
            }).then(function (data) {
                console.log(data.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    data
                });
            });
        }
        else {
            modelcontroller.Locations.findOne({
                where: {
                    id: req.body.Details
                }
            }).then(function (data) {
                console.log(data.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    data
                });
            });
        };
    }
    render(req, data.id);
    console.log(req.session);
}

exports.view_location = view_location;

//Edit Location Page
exports.edit_location = function (req, res) {
    console.log("Hit Edit Locations");
    let type = 'Location';
    modelcontroller.Locations.findOne({
        where: {
            id: req.body.Id
        }
    }).then(function (data) {
        res.render('new.pug', {
            successmessages: req.flash('successMessage'),
            failmessages: req.flash('failMessage'),
            user: req.user,
            name: 'Edit',
            type,
            data
        });
    })
    console.log(req.session);
}

//New Location Post
exports.new_location = function (req, res) {
    console.log('New Location Process #1 ' +req.body.Id);
    var existingid = '';
    modelcontroller.Locations
        .findOne({ where: { Name: req.body.Name } }).then(function (data) {
            if (data) {existingid = data.id};
            if (String(req.body.Id)!==String(existingid)) {
                console.log('failMessage', `Location ${data.Name} already exists.`);
                console.log(existingid);
                req.flash('failMessage', `Location ${data.Name} already exists.`);
                view_location(req, res, data);
            }
            else {
                LocationsUpsert({
                    Id: req.body.Id,
                    Name: req.body.Name,
                    Trust: req.body.Trust,
                    Address: req.body.Address,
                    Postcode: req.body.Postcode,
                    Telephone: req.body.Telephone,
                    Url: req.body.URL,
                    hlcid: req.user.hlcid
                },
                    {
                        id: req.body.Id
                    }).then(newLocation => {
                        req.flash('successMessage', `Location ${newLocation.Name} has been Created/Updated.`);
                        res.redirect('/locations');
                        console.log(`Location ${newLocation.Name}, with id ${newLocation.id} has been Created/Updated.`);
                    });
            }
        })
}

function LocationsUpsert(values, condition) {
    return modelcontroller.Locations
        .findOne({ where: condition })
        .then(function (obj) {
            if (obj) { // update
                return obj.update(values);
            }
            else { // insert
                return modelcontroller.Locations.create(values);
            }
        })
};