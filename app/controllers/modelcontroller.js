var exports = module.exports = {}

var models = require("../models");

//Declare Models
const HLC = models.hlc;
const Locations = models.locations;
const Users = models.user;
const Location_Contacts = models.location_contacts;
const Tasks = models.tasks;
const Presentations = models.presentations;
const LoginHistory = models.loginhistory;


exports.HLC = HLC;
exports.Locations = Locations;
exports.Users = Users;
exports.Location_Contacts = Location_Contacts;
exports.Tasks = Tasks;
exports.Presentations = Presentations;
exports.LoginHistory= LoginHistory;

//Create Relations 
HLC.hasMany(Locations, { foreignKey: "id" });
HLC.hasMany(Users, { foreignKey: "id" });
Locations.hasMany(Location_Contacts, { foreignKey: "Locationid" });
Locations.hasMany(Tasks, { foreignKey: "Locationid" });
Locations.hasMany(Presentations, { foreignKey: "Locationid" });
Users.belongsTo(HLC, { foreignKey: "hlcid" });
Users.hasMany(Tasks, { foreignKey: "userid" });
Users.hasMany(LoginHistory, { foreignKey: "userid" });
Users.hasMany(Presentations, { foreignKey: "userid" });
Locations.belongsTo(HLC, { foreignKey: "hlcid" });
Location_Contacts.hasMany(Tasks, { foreignKey: "Locationcontactid" });
Location_Contacts.hasMany(Presentations, { foreignKey: "Locationcontactid" });
Location_Contacts.belongsTo(Locations, { foreignKey: "Locationid" });
Tasks.belongsTo(Locations, { foreignKey: "Locationid" });
Tasks.belongsTo(Users, { foreignKey: "userid" });
Tasks.belongsTo(Location_Contacts, { foreignKey: "Locationcontactid" });
Presentations.belongsTo(Locations, { foreignKey: "Locationid" });
Presentations.belongsTo(Users, { foreignKey: "userid" });
Presentations.belongsTo(Location_Contacts, { foreignKey: "Locationcontactid" });
LoginHistory.belongsTo(Users, { foreignKey: "userid" });