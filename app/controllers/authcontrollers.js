var exports = module.exports = {}
var models = require("../models");
var modelcontroller = require("./modelcontroller");
var bCrypt = require('bcrypt-nodejs');
const { isAdmin } = require("../routes/authfunctions");

//Login Page
exports.login = function (request, response) {
    response.render('login.pug', { messages: request.flash('loginMessage'), email: request.flash('email') })
}

//New Entity Page
exports.new_entity = function (req, res) {
    let type = req.body.NewType;
    if (type === 'Contact') {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: {
                hlcid: req.user.hlcid
            }
        }).then(function (locationdata) {
            res.render('new.pug', {
                messages: req.flash('anyMessage'),
                user: req.user,
                name: 'New',
                type,
                locationdata
            });
        })
    }
    else if (type === 'Task') {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: {
                hlcid: req.user.hlcid
            }
        }).then(function (locationdata) {
            modelcontroller.Location_Contacts.findAll({
                attributes: ['Name', 'id'],
                order: ['Name']
            }).then(function (contactdata) {
                modelcontroller.Users.findAll({
                    order: ['Firstname'],
                    where: {
                        hlcid: req.user.hlcid
                    }
                }).then(function (userdata) {
                    res.render('new.pug', {
                        messages: req.flash('anyMessage'),
                        user: req.user,
                        name: 'New',
                        type,
                        locationdata,
                        contactdata,
                        userdata
                    });
                })
            })
        })
    }
    else if (type === 'Presentation') {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: {
                hlcid: req.user.hlcid
            }
        }).then(function (locationdata) {
            modelcontroller.Location_Contacts.findAll({
                attributes: ['Name', 'id'],
                order: ['Name']
            }).then(function (contactdata) {
                modelcontroller.Users.findAll({
                    order: ['Firstname'],
                    where: {
                        hlcid: req.user.hlcid
                    }
                }).then(function (userdata) {
                    res.render('new.pug', {
                        messages: req.flash('anyMessage'),
                        user: req.user,
                        name: 'New',
                        type,
                        locationdata,
                        contactdata,
                        userdata
                    });
                })
            })
        })
    }
    else {
        let data = null;
        res.render('new.pug', {
            messages: req.flash('anyMessage'),
            user: req.user,
            name: 'New',
            type,
            data
        });
    }
    console.log(req.session);
    console.log("Hit /new");
}

exports.changepassword = function (req, res) {
    console.log(req.body.newpassword1);
    if (req.body.newpassword1 == req.body.newpassword2) {
        return modelcontroller.Users
            .findOne({
                where: {
                    id: req.user.id
                }
            })
            .then(function (obj) {
                if (obj) { // update
                    console.log('Updating User Password Process #1 ');
                    var salt = bCrypt.genSaltSync(10);
                    return obj.update({
                        password: bCrypt.hashSync(req.body.newpassword1, salt)
                    }).then(updateduser => {
                        req.flash('successMessage', `User ID ${updateduser.id}'s Password updated.`);
                        res.redirect('/locations');
                    });
                }
                else {
                    req.flash('failMessage', `User does not exist.`);
                    res.redirect('/locations');
                }
            })
    }
    else {
        req.flash('failMessage', `Passwords did not match.`);
        res.redirect('/locations');
    }
};

exports.changepwdpage = function (req, res) {
    res.render('changepwd.pug', {
        messages: req.flash('anyMessage'),
        user: req.user
    })
};

exports.delete_entity = function (req, res) {
    let type = req.body.Type;
    let id = req.body.Id;
    if (isAdmin) {
        console.log("Deleting entity of type " + type + " and Id " + id)
        if (type === 'Location') {
            console.log("Location!")
        }
        else if (type === 'Contact') {
            console.log("Contact!")
        }
        else if (type === 'Task') {
            console.log("Task!")
        }
        else if (type === 'Presentation') {
            console.log("Presentation!")
            modelcontroller.Presentations.destroy({
                where: { id: id }
            })
            res.redirect('/presentations');
        }
    }
    else {
        console.log(`User Id:${req.user.id} tried and failed to delete ${type} of Id:${req.body.Id}`)
        req.flash('failMessage', `Only Admin Users can Delete Things.`);
        res.redirect('/locations')
    }
};

//Logout Function
exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.redirect('/login');
    });
};

exports.globchangepass = function (user, pass) {
    return modelcontroller.Users
        .findOne({
            where: {
                id: user
            }
        })
        .then(function (obj) {
            if (obj) { // update
                console.log('Updating User Password Process #1 ');
                var salt = bCrypt.genSaltSync(10);
                return obj.update({
                    password: bCrypt.hashSync(pass, salt)
                }).then(updateduser => {
                    console.log(`User ID ${updateduser.id}'s Password updated.`);
                });
            }
            else {
                console.log(`User does not exist.`);
            }
        })
};