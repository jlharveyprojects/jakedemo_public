var exports = module.exports = {}
var modelcontroller = require("./modelcontroller");

//Tasks Page
exports.tasks = function (req, res) {
    modelcontroller.Tasks.findAll({
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (tasks) {
        console.log(tasks);
        res.render('tasks.pug', {
            successmessages: req.flash('successMessage'),
            failmessages: req.flash('failMessage'),
            user: req.user,
            name: 'Tasks',
            tasks
        })
    });
    console.log(req.session);
    console.log("Hit /auth/tasks");
}

//TasksCal Page
exports.taskscal = function (req, res) {
    res.render('tasks.pug', {
        successmessages: req.flash('successMessage'),
        failmessages: req.flash('failMessage'),
        user: req.user,
        name: 'Tasks',
        type: 'Calendar'
    })
    console.log(req.session);
    console.log("Hit /auth/taskscal");
};

//Calendar JSON Server
exports.tasksJSON = function (req, res) {
    modelcontroller.Tasks.findAll({
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (tasks) {
        events = [];
        for (task in tasks) {
            console.log(tasks[task].duedate);
            events.push({
                "id": tasks[task].id,
                "title": tasks[task].location_contact.Name,
                "start": tasks[task].duedate,
                "textColor": '#000000'
            })
        }
    });
    res.json(events);
}

var view_task = function (req, res, data) {
    console.log("Hit View Task");
    let type = 'Task';
    let render = function (req, givenid) {
        if (givenid & !req.body.Id) {
            modelcontroller.Tasks.findOne({
                where: {
                    id: givenid
                },
                include: [
                    {
                        model: modelcontroller.Locations,
                        where: {
                            hlcid: req.user.hlcid
                        },
                    },
                    {
                        model: modelcontroller.Location_Contacts
                    },
                    {
                        model: modelcontroller.Users
                    }
                ]
            }).then(function (tasks) {
                console.log(tasks.location.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    tasks
                });
            })
        }
        else {
            modelcontroller.Tasks.findOne({
                where: {
                    id: req.body.Details
                },
                include: [
                    {
                        model: modelcontroller.Locations,
                        where: {
                            hlcid: req.user.hlcid
                        },
                    },
                    {
                        model: modelcontroller.Location_Contacts
                    },
                    {
                        model: modelcontroller.Users
                    }
                ]
            }).then(function (tasks) {
                console.log(tasks.location.Name);
                res.render('new.pug', {
                    successmessages: req.flash('successMessage'),
                    failmessages: req.flash('failMessage'),
                    user: req.user,
                    name: 'View',
                    type,
                    tasks
                });
            })
        }
    }
    render(req, data.id);
    console.log(req.session);
}

//View Task Page
exports.view_task = view_task;

//Edit Contact Page
exports.edit_task = function (req, res) {
    console.log("Hit Edit task");
    let type = 'Task';
    modelcontroller.Tasks.findOne({
        where: {
            id: req.body.Id
        },
        include: [
            {
                model: modelcontroller.Locations,
                where: {
                    hlcid: req.user.hlcid
                },
            },
            {
                model: modelcontroller.Location_Contacts
            },
            {
                model: modelcontroller.Users
            }
        ]
    }).then(function (tasks) {
        modelcontroller.Locations.findAll({
            attributes: ['Name', 'id'],
            order: ['Name'],
            where: {
                hlcid: req.user.hlcid
            }
        }).then(function (locationdata) {
            modelcontroller.Location_Contacts.findAll({
                attributes: ['Name', 'id'],
                order: ['Name']
            }).then(function (contactdata) {
                modelcontroller.Users.findAll({
                    order: ['Firstname'],
                    where: {
                        hlcid: req.user.hlcid
                    }
                }).then(function (userdata) {
                    res.render('new.pug', {
                        successmessages: req.flash('successMessage'),
                        failmessages: req.flash('failMessage'),
                        user: req.user,
                        name: 'Edit',
                        type,
                        tasks,
                        locationdata,
                        contactdata,
                        userdata
                    });
                })
            })
        })
    })
    console.log(req.session);
}

//New Task Post
exports.new_task = function (req, res) {
    console.log('New Task Process #1 ' + req.body.Id);
    TasksUpsert({
        id: req.body.Id,
        duedate: req.body.DueDate,
        Locationid: req.body.Location,
        Locationcontactid: req.body.Contact,
        action: req.body.Action,
        type: req.body.Type,
        notes: req.body.Notes,
        userid: req.body.User
    },
        {
            id: req.body.Id
        }).then(newTask => {
            req.flash('successMessage', `Task ${newTask.action} has been Created/Updated.`);
            res.redirect('/tasks');
            console.log(`Task ${newTask.action}, with id ${newTask.id} has been Created/Updated.`);
        });
}

function TasksUpsert(values, condition) {
    return modelcontroller.Tasks
        .findOne({ where: condition })
        .then(function (obj) {
            if (obj) { // update
                console.log('Updating Task Process #1 ')
                return obj.update(values);
            }
            else { // insert
                console.log('Updating Task Process #2 ')
                return modelcontroller.Tasks.create(values);
            }
        })
};