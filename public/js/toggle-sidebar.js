$(document).ready(function() {
    $("#sidebarToggle").click(function(e) {
        e.preventDefault();
        
        $("#sidebar-wrapper").toggleClass("toggled");

        $('#sidebar-wrapper.toggled').find("#sidebar-wrapper").find(".collapse").collapse('hide');
        
    });
});