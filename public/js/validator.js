jQuery.validator.addMethod("notEqualTo", function (v, e, p) {
    return this.optional(e) || v != p;
}, "Please enter a valid Value");

$(document).ready(function () {
    $('#location').validate({ // initialize the plugin
        errorElement: 'span',
        errorClass: 'alert-danger',
        errorPlacement: function(error, element) {},
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        onkeyup: false,
        errorPlacement: function (error, element) { error.insertAfter(element); },
        rules: {
            Name: {
                required: true
            }
        }
    });
});

$(document).ready(function () {
    $('#contact').validate({ // initialize the plugin
        errorElement: 'span',
        errorClass: 'alert-danger',
        errorPlacement: function(error, element) {},
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        onkeyup: false,
        errorPlacement: function (error, element) { error.insertAfter(element); },
        rules: {
            Email: {
                email: true
            },
            Type: {
                notEqualTo: "--Contact Type"
            },
            Location: {
                required: true,
                notEqualTo: "--Location"
            }
        }
    });
});

$(document).ready(function () {
    $('#task').validate({ // initialize the plugin
        errorElement: 'span',
        errorClass: 'alert-danger',
        errorPlacement: function(error, element) {},
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        onkeyup: false,
        errorPlacement: function (error, element) { error.insertAfter(element); },
        rules: {
            Location: {
                required: true,
                notEqualTo: "--Location"
            },
            Contact: {
                required: true,
                notEqualTo: "--Contact"
            },
            User: {
                required: true,
                notEqualTo: "--User"
            },
            Type: {
                required: true,
                notEqualTo: "--Task Type"
            },
            Action: {
                required: true,
                notEqualTo: "--Task Action"
            }
        }
    });
});

$(document).ready(function () {
    $('#presentation').validate({ // initialize the plugin
        errorElement: 'span',
        errorClass: 'alert-danger',
        errorPlacement: function(error, element) {},
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(this.settings.errorElementClass).removeClass(errorClass);
        },
        onkeyup: false,
        errorPlacement: function (error, element) { error.insertAfter(element); },
        rules: {
            Location: {
                required: true,
                notEqualTo: "--Location"
            },
            Contact: {
                required: true,
                notEqualTo: "--Contact"
            },
            User: {
                required: true,
                notEqualTo: "--User"
            }
        }
    });
});