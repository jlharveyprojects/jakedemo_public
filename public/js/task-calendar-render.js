document.addEventListener('DOMContentLoaded', function () {
  var calendarEl = document.getElementById('taskcalendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    plugins: ['dayGrid', 'interaction', 'timegrid'],
    eventClick: function (info) {

      var eventObj = info.event;
      window.open('', 'TheWindow');
      $('<form id="TheForm" action="/tasks/details" method="POST" target="TheWindow"><input type="hidden" id="formInput" name="Details"></form"').appendTo('body')
      document.getElementById("formInput").value = eventObj.id;
      document.getElementById('TheForm').submit();
      
    },
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    }
    , events: '/taskCalendarJSON/'
    , height: 650

  });

  calendar.refetchEvents()
  calendar.render();
});