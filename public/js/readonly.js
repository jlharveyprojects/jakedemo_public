$(document).ready(function(){
    /*Disable all input type="text" box*/
    $('#location input').prop("readonly", true);
    $('#location').attr('action', '/editlocation');
    /*Disable textarea using id 
    $('#location #txtAddress').prop("disabled", true); */
    $('#contact input[type="text"]').prop("readonly", true);
    $('#contact select[name="Location"]').prop('disabled', true);
    $('#contact select[name="Type"]').prop('disabled', true);
    $('#contact').attr('action', '/editcontact');
    $('#task input').prop("readonly", true);
    $('#task textarea').prop("readonly", true);
    $('#task select').prop('disabled', true);
    $('#task').attr('action', '/edittask');
    $('#presentation input').prop("readonly", true);
    $('#presentation textarea').prop("readonly", true);
    $('#presentation select').prop('disabled', true);
    $('#presentation').attr('action', '/editpresentation');
});