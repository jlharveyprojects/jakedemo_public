jQuery.datetimepicker.setLocale('en-GB');

jQuery(document).ready(function () {
  jQuery(function () {
    jQuery('#datetimepicker').focus(function () {
      jQuery('#datetimepicker').datetimepicker({
        inline: true,
        sideBySide: true
      });
    });
    jQuery('#datepicker').focus(function () {
      jQuery('#datepicker').datetimepicker({
        inline: true,
        sideBySide: true,
        timepicker: false
      });
    });
  });
});

