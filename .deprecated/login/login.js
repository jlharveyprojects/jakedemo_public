var express = require('express');
var session = require('express-session');
var login = express.Router();
var bodyParser = require('body-parser');
var path = require('path');
var BetterMemoryStore = require('session-memory-store')(session);
var store = new BetterMemoryStore({ expires: 60 * 60 * 1000, debug: true });
var authController = require('../../app/controllers/authcontrollers.js');

login.use(session({
	name: 'JSESSION',
    secret: 'MYSECRETISVERYSECRET',
    store: store,
    resave: true,
    saveUninitialized: true
}));
login.use(bodyParser.urlencoded({extended : true}));
login.use(bodyParser.json());

login.get('/', authController.login);

module.exports = login;
