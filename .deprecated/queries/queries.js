module.exports = {
    insert_todo: `INSERT INTO tbl_todo(title, completed) VALUES(?, ?)`,
    read_todo: `SELECT * FROM users`,
    update_todo: `UPDATE tbl_todo SET tbl_todo.title = ?, tbl_todo.completed = ? WHERE tbl_todo.id = ?`,
    delete_todo: `DELETE FROM tbl_todo WHERE tbl_todo.id = ?`,
    //users
    login_user: `SELECT * FROM users WHERE Email = ? AND password = ?`,
    query_user: `SELECT * from users where Email = ?`,
    query_user_from_id: `SELECT * from users where id = ?`,
    //locations
    query_locations : `SELECT Name,Address,Postcode,Telephone from locations where hlcid = ?`
}