var LocalStrategy = require('passport-local').Strategy;

var DBquery = require('../../../.deprecated/DAO/usersDAO.js');
var dbquery = new DBquery();

var myLocalConfig = (passport) => {
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user);
        console.log("User ID Serialized:"+user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (user, done) {
        dbquery.queryUserfromID({id: user.id}, function (user) {
            console.log("User ID De-Serialized:"+user.id);
            done(null, user[0]);
        });
    });

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true //passback entire req to call back
    }, function (req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function () {
            dbquery.queryentity({
                email: email
            }, function (res) {
                    // if no user is found, return the message
                    if (!res) {
                        console.log("Passport No Rows returned");
                        return done(null, false, req.flash('loginMessage', 'That email address does not exist'), req.flash('email', email));
                    }
                    else {
                        console.log({ "dbPassword": res[0].password });
                        if ((res[0].password == password)) {
                            console.log("local-login else stage")
                            return done(null, res[0]);
                        }
                        // all is gone bad, return user
                        else {
                            console.log("local-login password error")
                            return done(null, false, req.flash('loginMessage', 'That password is incorrect'), req.flash('email', email));
                        }
                    }
                });
        });

        }));

// =========================================================================
// LOCAL SIGNUP ============================================================
// =========================================================================
passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
    },
    function(req, email, password, done) {
        if (email)
            email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching

        // asynchronous
        process.nextTick(function() {
            // if the user is not already logged in:
            if (!req.user) {
                User.findOne({ 'local.email' :  email }, function(err, user) {
                    // if there are any errors, return the error
                    if (err)
                        return done(err);

                    // check to see if theres already a user with that email
                    if (user) {
                        return done(null, false);
                    } else {

                        // create the user
                        var newUser            = new User();

                        newUser.local.email    = email;
                        newUser.local.password = newUser.generateHash(password);

                        newUser.save(function(err) {
                            if (err)
                                return done(err);

                            return done(null, newUser);
                        });
                    }

                });
                // if the user is logged in but has no local account...
            } else if ( !req.user.local.email ) {
                // ...presumably they're trying to connect a local account
                // BUT let's check if the email used to connect a local account is being used by another user
                User.findOne({ 'local.email' :  email }, function(err, user) {
                    if (err)
                        return done(err);

                    if (user) {
                        return done(null, false);
                        // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                    } else {
                        var user = req.user;
                        user.local.email = email;
                        user.local.password = user.generateHash(password);
                        user.save(function (err) {
                            if (err)
                                return done(err);

                            return done(null,user);
                        });
                    }
                });
            } else {
                // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                return done(null, req.user);
            }

        });

    }));
};

module.exports = myLocalConfig;