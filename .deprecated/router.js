var express = require('express');
var router = express.Router();
var dbquery = require('./db_connection.js');
var mysql = require('mysql');
var sql = "select ?? from locations;";
var sqlquery = mysql.format(sql, ["Address"]);

router.get('/', function(req, res){
   res.send('GET route on hellothings.');
});
router.post('/', function(req, res){
   res.send('POST route on hellothings.');
});

router.get('/test', function(req, res){
-   res.send('TEST route on hellothings.');
});
router.get('/test/:id', function(req, res){
   res.send('TEST id you specified is ' + req.params.id);
});
router.get('/dbresult', function(req, res){
   dbquery.query_db(sqlquery,res);
});
router.get('/pug', function(req, res){
   res.render('index',
      {title:'JakeTitle',message:'Welcome'})
});
router.get('/content', function(req, res){
   res.render('content')
});
//export this router to use in our index.js
module.exports = router;