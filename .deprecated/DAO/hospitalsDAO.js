const dbConnection = require("../db_connection.js");
const queries = require("../queries/queries.js");

module.exports = class TodoDao {
    async query_locations(entity, res) {
      let con = await dbConnection();
      try {
        await con.query("START TRANSACTION");
        await con.query(queries.query_locations,[entity.hlcid], (err, rows) => {
            if (!rows.length){
              console.log("locationsDAO No Rows returned");
              res (null);
            }
            else if (err) {
              console.log(err);
            }
            else {
              console.log(rows);
              console.log("query_locations result:"+rows);
              return (rows);
            }
          });
        await con.query("COMMIT");
      } catch (ex) {
        //await con.query("ROLLBACK");
        console.log(ex);
        throw ex;
      } finally {
        await con.release();
        await con.destroy();
      }
    }
  };