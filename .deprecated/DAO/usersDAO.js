const dbConnection = require("../db_connection.js");
const queries = require("../queries/queries.js");

module.exports = class TodoDao {
    async loginentity(entity, res) {
      let con = await dbConnection();
      try {
        await con.query("START TRANSACTION");
        await con.query(queries.login_user,[entity.email, entity.password], (err, rows) => {
            if (!err) {
              console.log(rows);
              res (rows);
            }
            if (err) {
              console.log(err);
            }
          });
        await con.query("COMMIT");
      } catch (ex) {
        //await con.query("ROLLBACK");
        console.log(ex);
        throw ex;
      } finally {
        await con.release();
        await con.destroy();
      }
    }

    async queryentity(entity, res) {
      let con = await dbConnection();
      try {
        await con.query("START TRANSACTION");
        await con.query(queries.query_user,[entity.email], (err, rows) => {
            if (!rows.length){
              console.log("DAO No Rows returned")
              res (null);
            }
            else if (err) {
              console.log(err);
            }
            else {
              console.log(rows);
              console.log("queryentity result Email:"+rows[0].Email);
              res (rows);
            }
          });
        await con.query("COMMIT");
      } catch (ex) {
        //await con.query("ROLLBACK");
        console.log(ex);
        throw ex;
      } finally {
        await con.release();
        await con.destroy();
      }
    }

    async queryUserfromID(entity, res) {
      let con = await dbConnection();
      try {
        await con.query("START TRANSACTION");
        await con.query(queries.query_user_from_id,[entity.id], (err, rows) => {
            if (!rows.length){
              console.log("DAO No Rows returned");
              res (null);
            }
            else if (err) {
              console.log(err);
            }
            else {
              console.log(rows);
              console.log("queryUserfromID result Id:"+rows[0].id);
              res (rows);
            }
          });
        await con.query("COMMIT");
      } catch (ex) {
        //await con.query("ROLLBACK");
        console.log(ex);
        throw ex;
      } finally {
        await con.release();
        await con.destroy();
      }
    }
  };