var mysql = require('promise-mysql');

var dbConfig = {
  connectionLimit: 100, //important
  host: "digitalmediatraining.co.uk",
  user: "digita34_hlc",
  password: "Location1",
  database: "digita34_hlc"
};

module.exports = async () => {
  try {
      let pool;
      let con;
      if (pool) con = pool.getConnection();
      else {
          pool = await mysql.createPool(dbConfig);
          con = pool.getConnection();
      }
      return con;
  } catch (ex) {
      throw ex;
  }
}